using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public  class Controller_AmigoV2 : MonoBehaviour
{
    public GameObject jugadorMontado;

    private float alturaSobreMontura=1;
    
    public float tiempoEsperaSoltar = 0.1f;
    private float tiempoUltimaPulsacionS = 0.0f;
    void Start()
    {
        
    }

  
    void Update()
    {

        if (this.jugadorMontado != null)
        {
           
                agarrarPlayer();
            
            //montura mas alta para players altos
            if (jugadorMontado.GetComponent<Controller_Player>().playerNumber == 2 || jugadorMontado.GetComponent<Controller_Player>().playerNumber == 3)
            {
                alturaSobreMontura = 3;
            }
            else
            {
                alturaSobreMontura = 1;
            }


            


            //salida, soltar player
            if (Input.GetKey(KeyCode.S) && (Time.time - tiempoUltimaPulsacionS) >= tiempoEsperaSoltar)
            {
                soltarPlayer();
            }

        }
       


      
        
    }

    void OnCollisionStay(Collision collision)
    {
        GameObject objeto= collision.gameObject;
        if (Input.GetKey(KeyCode.S) && ((Time.time - tiempoUltimaPulsacionS) >= tiempoEsperaSoltar) )
        {
            if (objeto.GetComponent<Controller_Player>())
            {
                jugadorMontado = collision.gameObject;
                tiempoUltimaPulsacionS = Time.time;

            }
           
        }

    }

    private void agarrarPlayer()
    {
        jugadorMontado.GetComponent<BoxCollider>().isTrigger = true;
       
      jugadorMontado.transform.position = this.transform.position + new Vector3(0, alturaSobreMontura, 0);

       
    }
    private void soltarPlayer()
    {
        jugadorMontado.GetComponent<BoxCollider>().isTrigger = false;
        jugadorMontado = null;

        tiempoUltimaPulsacionS = Time.time;
    }
}
