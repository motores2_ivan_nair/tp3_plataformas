using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Boton : MonoBehaviour
{

    public Controller_Puerta controlPuerta;

    private void Start()
    {
        controlPuerta = gameObject.GetComponentInChildren<Controller_Puerta>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other != null   &&  other.gameObject.CompareTag("Caja") )
        {
            controlPuerta.abrirPuerta();
        }
        else 
        {
            controlPuerta.cerrarPuerta();
        }
    }
}
