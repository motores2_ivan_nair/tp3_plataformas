using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Enemy : MonoBehaviour
{
    public float velCaida;
    public float detectarDistancia;
    private Transform player;
    private bool isFalling = false;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        isFalling = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (!isFalling)
        {

            RaycastHit hit;
            if (Physics.Raycast(transform.position, Vector3.down, out hit, detectarDistancia))
            {
                if (hit.collider.CompareTag("Player"))
                {
                    isFalling = true;
                }
            }
        }
        else
        {
            transform.Translate(Vector3.down * velCaida * Time.deltaTime);
            //transform.position -= Vector3.up * velCaida * Time.deltaTime;
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            isFalling = false;
            Destroy(gameObject);
            
        }
        if(collision.gameObject.CompareTag("Player"))
        {
            GameManager.gameOver = true;
        }
    }

}
