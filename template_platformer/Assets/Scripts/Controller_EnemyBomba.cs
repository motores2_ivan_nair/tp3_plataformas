using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_EnemyBomba : MonoBehaviour
{
    public float detectarDistancia;
    public GameObject bomba;
    private Transform player;
    public float bombaSpawn;
    private float timer;
    

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= bombaSpawn)
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, Vector3.down, out hit, detectarDistancia))
            {
                if (hit.collider.CompareTag("Player"))
                {
                    Instantiate(bomba, transform.position, Quaternion.identity);
                    Invoke("destruirBomba", 2f);
                }
            }
            timer = 0f;
        }
    }

    private void destruirBomba()
    {
        Destroy(bomba);
    }
}
