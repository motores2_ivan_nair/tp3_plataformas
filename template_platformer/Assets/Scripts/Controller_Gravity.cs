using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Gravity : MonoBehaviour
{
    public float gravedad;
    private bool invertido;
    private Rigidbody rb;
    private float gravedadOriginal;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        gravedadOriginal = rb.velocity.y;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            invertido = !invertido;
            if (invertido)
            {
                rb.velocity = new Vector3(rb.velocity.x, gravedad, rb.velocity.z);
                rb.useGravity = false;
                Physics.gravity = -Vector3.up * gravedad;
            }
            else
            {
                rb.velocity = new Vector3(rb.velocity.x, gravedadOriginal, rb.velocity.z);
                rb.useGravity = true;
                Physics.gravity = Vector3.down * gravedad;
            }
        }
    }
}
