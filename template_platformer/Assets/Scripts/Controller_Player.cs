﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    public float jumpForce = 10;

    public float speed = 5;

    public int playerNumber;

    public Rigidbody rb;

    private BoxCollider col;

    public LayerMask floor;

    public GameObject identificador;

    internal RaycastHit leftHit,rightHit,downHit;

    public float distanceRay,downDistanceRay;

    private bool canMoveLeft, canMoveRight,canJump;
    internal bool onFloor;

    public GameObject box;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<BoxCollider>();
        rb.constraints = RigidbodyConstraints.FreezePositionX| RigidbodyConstraints.FreezePositionZ|RigidbodyConstraints.FreezeRotation;
        
    }

    public virtual void FixedUpdate()
    {
        if (GameManager.actualPlayer == playerNumber)
        {
            Movement();


            //// hace una zona tipo caja alrededor del player, que detecta colliders
            //Collider[] hitColliders = Physics.OverlapBox(transform.position, new Vector3(1f, 1f, 1f));
            //bool isNearBox = false;
           
            ////recorre los colliders en busca de cajas
            //for (int i = 0; i < hitColliders.Length; i++)
            //{
            //    if (hitColliders[i].gameObject.CompareTag("Caja"))
            //    {
            //        box = hitColliders[i].gameObject;
            //        isNearBox = true;
            //        break;
            //    }
            //}

            ////si hay caja entonces se da velocidad a la caja
            //if (isNearBox)
            //{
            //    if (Input.GetKey(KeyCode.E))
            //    {
                    
                    
            //        Rigidbody boxRigidbody = box.GetComponent<Rigidbody>();
            //        boxRigidbody.velocity = new Vector3(rb.velocity.x, boxRigidbody.velocity.y, rb.velocity.z);
            //    }
            //}
        }
    }

    private void Update()
    {

        if (GameManager.actualPlayer == playerNumber)
        {

            if (GameObject.Find("identificador")!=null)
            { 
            GameObject.Find("identificador").transform.position = transform.position + new Vector3(0f, 2f, -2f);
            }
            
            else
            {
                GameObject identificadorPrefab = Instantiate(identificador, transform.position, Quaternion.identity);
                identificadorPrefab.name = "identificador";

            }
            

            Jump();
            if (SomethingLeft())
            {
                canMoveLeft = false;
            }
            else
            {
                canMoveLeft = true;
            }
            if (SomethingRight())
            {
                canMoveRight = false;
            }
            else
            {
                canMoveRight = true;
            }

            if (IsOnSomething())
            {
                canJump = true;
            }
            else
            {
                canJump = false;
            }

        }
        else
        {
            if (onFloor)
            {
                rb.constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotation;
            }
            else
            {
                if (IsOnSomething())
                {
                    if (downHit.collider.gameObject.CompareTag("Player"))
                    {
                        rb.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
                    }
                }
            }
        }
    }


    public virtual bool IsOnSomething()
    {
        return Physics.BoxCast(transform.position, new Vector3(transform.localScale.x * 0.9f, transform.localScale.y/3,transform.localScale.z*0.9f), Vector3.down, out downHit, Quaternion.identity, downDistanceRay);
    }

    public virtual bool SomethingRight()
    {
        Ray landingRay = new Ray(new Vector3(transform.position.x,transform.position.y-(transform.localScale.y / 2.2f),transform.position.z), Vector3.right);
        Debug.DrawRay(landingRay.origin, landingRay.direction, Color.green);

        
        bool hayAlgo = Physics.Raycast(landingRay, out rightHit, transform.localScale.x/1.8f);


        if (rightHit.collider != null && (rightHit.collider.CompareTag("Caja") ||
            rightHit.collider.CompareTag("PuertaAbierta") ||
            rightHit.collider.CompareTag("Sierra") ||
            rightHit.collider.CompareTag("ZonaBombas") ||
            rightHit.collider.CompareTag("zonaBombasHielo"))    )
        {
            return false;
        }

        else
        {
            return hayAlgo;
        }
    }

    public virtual bool SomethingLeft()
    {
        Ray landingRay = new Ray(new Vector3(transform.position.x, transform.position.y - (transform.localScale.y/2.2f), transform.position.z), Vector3.left);
        Debug.DrawRay(landingRay.origin, landingRay.direction, Color.green);
       
        bool hayAlgo=Physics.Raycast(landingRay, out leftHit, transform.localScale.x/1.8f);

        if (leftHit.collider != null && (   leftHit.collider.CompareTag("Caja")    ||
            leftHit.collider.CompareTag("PuertaAbierta") ||
            leftHit.collider.CompareTag("Sierra") ||
            leftHit.collider.CompareTag("ZonaBombas") ||
            leftHit.collider.CompareTag("zonaBombasHielo"))      )
        {
            return false;
        }

        else
        {
            return hayAlgo;
        }
    }

    private void Movement()
    {
        if (Input.GetKey(KeyCode.A) && canMoveLeft)
        {
                rb.velocity = new Vector3(1 * -speed , rb.velocity.y, 0);
        }
        else if (Input.GetKey(KeyCode.D) && canMoveRight)
        {
                rb.velocity = new Vector3(1 * speed, rb.velocity.y, 0);
        }
        else
        {
            rb.velocity = new Vector3(0, rb.velocity.y, 0);
        }
        //if (!canMoveLeft)
        //    rb.velocity = new Vector3(0, rb.velocity.y, 0);
        //if (!canMoveRight)
        //    rb.velocity = new Vector3(0, rb.velocity.y, 0);
    }

    public virtual void Jump()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            if (canJump)
            {
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    private void freezearPlayer()
    {
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePosition| RigidbodyConstraints.FreezeRotation;
    }
    private void resetFreeze()
    {
      GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation| RigidbodyConstraints.FreezePositionZ;
    }

    public virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Water") && GameManager.actualPlayer != 3)
        {
            Destroy(this.gameObject);
            GameManager.gameOver = true;
        }

        if (collision.gameObject.CompareTag("Sierra")   &&  GameManager.actualPlayer!=9)
        {
            Destroy(this.gameObject);
            GameManager.gameOver = true;
        }


        if (collision.gameObject.CompareTag("Bomba"))
        {
            Destroy(this.gameObject);
            GameManager.gameOver = true;
        }

        if (collision.gameObject.CompareTag("BombasHielo"))
        {
            freezearPlayer();
            Invoke("resetFreeze", 5);
        }

        if (collision.gameObject.CompareTag("Floor"))
        {
            onFloor = true;
        }

    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            onFloor = false;
        }
    }
}
