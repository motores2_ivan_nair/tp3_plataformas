using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player_Amigo : MonoBehaviour
{

    private GameObject otroPlayer;

    private bool imanUsable;

    public bool tocandoPlayer=false;

    private bool inputS;

    private Collision otroPlayerCollision;

    private float tiempoEspera = 0.5f;
    private void Start()
    {
        imanUsable = true;
        inputS = false;
    }

    private void Update()
    {
       

        if (Input.GetKeyDown(KeyCode.S) && tocandoPlayer && inputS == false)
        {

            //agarra al player

            if (otroPlayerCollision != null)
            {
                Iman(otroPlayerCollision);
            }
            else
            {
                Iman();
            }

            imanUsable = false;
            inputS = true;


            tiempoEspera = 0;
            //despues de 5 segundos lo suelta
            //Invoke("soltarIman", 10);

            //cd de 5 segundos para volver a usar
            //Invoke("resetIman", 5);

        }

        //un tiempo de espera para la siguiente condicion de inputS
        if (tiempoEspera < 0.9f)
        {
            tiempoEspera += Time.deltaTime;
        }


        if (tiempoEspera>=0.5f    && inputS == true && Input.GetKeyDown(KeyCode.S) )
        {
            soltarIman();
            Invoke("resetIman", 5);
           
            
            inputS = false;
        }




        //los players grandes se posicionan mas arriba
        if (otroPlayer.GetComponent<Controller_Player>().playerNumber == 3 || otroPlayer.GetComponent<Controller_Player>().playerNumber == 2)
        {
            otroPlayer.transform.position = transform.position + new Vector3(0, 3, 0);


        }
        else
        {
            otroPlayer.transform.position = transform.position + new Vector3(0, 1f, 0);
        }


    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<Controller_Player>()  &&  imanUsable         && GameManager.actualPlayer == 9)
        {
            tocandoPlayer = true;
            otroPlayerCollision = collision;

        }
        else
        {
            tocandoPlayer = false;
            otroPlayerCollision = null;
        }

        
        
       
    }

    private void Iman(Collision collision)
    {
        otroPlayer = collision.gameObject;
        otroPlayer.transform.SetParent(transform);
        otroPlayer.GetComponent<Rigidbody>().mass = 0;
    }

    private void Iman()
    {
        otroPlayer = null;    
    }
    private void soltarIman()
    {
        if (otroPlayer != null)
        {
            otroPlayer.transform.SetParent(null);
            otroPlayer.GetComponent<Rigidbody>().mass=1;
            otroPlayer = null;
        }
    }
    private void resetIman()
    {
        imanUsable = true;
    }


    
}
