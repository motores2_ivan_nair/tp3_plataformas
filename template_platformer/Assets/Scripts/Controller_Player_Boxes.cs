using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player_Boxes : MonoBehaviour
{
    public GameObject caja_prefab;
    public float yAgregada = 1;

    private Vector3 cajaPos;
    public int maxCajas;
    private int actualCajas;

    private bool cajaInstanciable;
    private int caja_a_Instanciar=1;
    private void Start()
    {
        cajaInstanciable = true;
    }

    void Update()
    {
        cajaPos = transform.position;
        cajaPos.y += yAgregada;


        if (caja_a_Instanciar > maxCajas)
        {
            caja_a_Instanciar = 1;
        }

        if (Input.GetKeyDown(KeyCode.S) && cajaInstanciable && GameManager.actualPlayer==7)
        {

            //si hay mas del max, la primera se vuelve la ultima
            if (actualCajas >= maxCajas)
            {
                //Destroy(GameObject.Find("caja1"));
                //actualCajas--;
                GameObject.Find("caja" + caja_a_Instanciar).transform.position = cajaPos;
                caja_a_Instanciar++;
            }


            if (actualCajas < maxCajas)
            {
                instanciarCajas(cajaPos);
            }
            //instanciar si hay menos del max


           


            Invoke("resetIntanciarCajas", 1);
        }

        

    }

    public void instanciarCajas(Vector3 position)
    {

        


        if (caja_prefab != null) 
        {
            GameObject instance = Instantiate(caja_prefab, position, Quaternion.identity);

            actualCajas ++;
            instance.name = "caja"+ actualCajas.ToString();
           

            cajaInstanciable=false;
        }
       
    }

    public void resetIntanciarCajas()
    {
        cajaInstanciable = true;
    }
}


