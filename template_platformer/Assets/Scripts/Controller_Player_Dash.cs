using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player_Dash : MonoBehaviour
{
    private bool puedeDash;
    private Rigidbody rb;
    public float dashSpeed;
    public float dashCD;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        puedeDash = true;
    }

    // Update is called once per frame
    void Update()
    {
        if ((Input.GetKey(KeyCode.D)) && Input.GetKeyDown(KeyCode.LeftShift) && puedeDash/* && GameManager.actualPlayer == 8*/)
        {
            rb.AddForce(new Vector3(dashSpeed * 10, 0, 0), ForceMode.Impulse);
            puedeDash = false;
            Invoke(nameof(ResetDash), dashCD);
        }
        if (Input.GetKey(KeyCode.A) && Input.GetKeyDown(KeyCode.LeftShift) && puedeDash/* && GameManager.actualPlayer == 8*/)
        {
            rb.AddForce(Vector3.left * dashSpeed * 10, ForceMode.Impulse);
            puedeDash = false;
            Invoke(nameof(ResetDash), dashCD);
        }
    }

    public void ResetDash()
    {
        puedeDash = true;
    }
}
