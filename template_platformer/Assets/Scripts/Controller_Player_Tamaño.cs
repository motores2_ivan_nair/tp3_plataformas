using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player_Tamaño : MonoBehaviour
{
    Controller_Player playerController;
    private float startEscalaY;
    private float startEscalaX;
    private Rigidbody rb;

    public float escalaChicoY;
    public float escalaChicoX;
    bool chico;

    public float escalaGrandeY;
    public float escalaGrandeX;
    bool grande;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        playerController = GetComponent<Controller_Player>();
        startEscalaY = transform.localScale.y;
        startEscalaX = transform.localScale.x;
        chico = false;
        grande = false;
    }

    // Update is called once per frame
    void Update()
    {
        tamañoInputs();
    }

    private void tamañoInputs()
    {
        if(Input.GetKeyDown(KeyCode.LeftControl) && GameManager.actualPlayer == 10)
        {
            transform.localScale = new Vector3(escalaChicoX, escalaChicoY, transform.localScale.z);
            chico = true;
            grande = false;
            playerController.jumpForce = 20;
            playerController.speed = 15; 
        }

        if (Input.GetKeyDown(KeyCode.LeftShift) && GameManager.actualPlayer == 10)
        {
            grande = true;
            chico = false;
            transform.localScale = new Vector3(escalaGrandeX, escalaGrandeY, transform.localScale.z);
            playerController.jumpForce = 10;
            playerController.speed = 7;
        }

        if (Input.GetKeyDown(KeyCode.F) && chico && GameManager.actualPlayer == 10 || Input.GetKeyDown(KeyCode.F) && grande && GameManager.actualPlayer == 10)
        {
            transform.localScale = new Vector3(startEscalaX, startEscalaY, transform.localScale.z);
            chico = false;
            grande = false;
            playerController.jumpForce = 15;
            playerController.speed = 10;
        }
    }
}
