using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Puerta : MonoBehaviour
{

    private Material mat;
    private Color normalColor;
    private void Start()
    {
        mat = GetComponent<Renderer>().material;
        normalColor = mat.color;
    }

    public void abrirPuerta()
    {
        mat.color = Color.green;
        gameObject.GetComponent<BoxCollider>().isTrigger = true;
        gameObject.tag = "PuertaAbierta";

    }

    public void cerrarPuerta()
    {
        mat.color = normalColor;
        gameObject.GetComponent<BoxCollider>().isTrigger = false;
        gameObject.tag = "Puerta";
    }
}
