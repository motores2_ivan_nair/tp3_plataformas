﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Target : MonoBehaviour
{
    public int targetNumber;

    public bool playerOnTarget;

    private void Start()
    {
        playerOnTarget = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (other.GetComponent<Controller_Player>().playerNumber == targetNumber)
            {
                playerOnTarget = true;
                //Debug.Log("P on T");
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (other.GetComponent<Controller_Player>().playerNumber == targetNumber)
            {
                playerOnTarget = false;
                //Debug.Log("P off T");
            }
        }
    }


    private void OnCollisionEnter(Collision collision)
    {
        BoxCollider boxCollider = GetComponent<BoxCollider>();
        if (boxCollider != null && !boxCollider.isTrigger && boxCollider.enabled)
        {

            if (collision.gameObject.CompareTag("Player"))
            {
                if (collision.gameObject.GetComponent<Controller_Player>().playerNumber == targetNumber)
                {
                    playerOnTarget = false;
                    //Debug.Log("P off T");
                }
            }
        }
    }
}
