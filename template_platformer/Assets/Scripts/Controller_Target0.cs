using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Target0 : MonoBehaviour
{
    public float moverseDerechaPos = 3;
    public float moverseIzquierdaPos = 3;

    public float moverseArribaPos = 3;
    public float moverseAbajoPos = 3;
    public float velocidad = 1;

    private bool moverseDerecha=true;
    private bool moverseArriba = true;

    private Rigidbody rb;
   

    private Vector3 posObjetivoDerecha;
    private Vector3 posObjetivoIzquierda;

    private Vector3 posObjetivoArriba;
    private Vector3 posObjetivoAbajo;
    void Start()
    {
        rb = GetComponent<Rigidbody>();

       

       
        posObjetivoDerecha = transform.position +  new Vector3(moverseDerechaPos, 0, 0) ;

        posObjetivoIzquierda = transform.position - new Vector3(moverseIzquierdaPos, 0, 0);

        posObjetivoArriba= transform.position+ new Vector3(0, moverseArribaPos, 0);

        posObjetivoAbajo = transform.position - new Vector3(0, moverseAbajoPos, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (moverseArribaPos != 0 && moverseAbajoPos != 0)
        {
            movArribaAbajo();
        }

        if (moverseDerechaPos != 0 && moverseIzquierdaPos != 0)
        {
            movDerIzq();
        }



    }


    public void MoverseDerecha()
    {
        Vector3 movimiento = transform.right * Time.deltaTime * velocidad;
        
        
        rb.MovePosition(transform.position + movimiento);

       
    }
    public void MoverseIzquierda()
    {
        Vector3 movimiento = transform.right * Time.deltaTime * velocidad;


        rb.MovePosition(transform.position - movimiento);
    }

    public void MoverseArriba()
    {
        Vector3 movimiento = transform.up * Time.deltaTime * velocidad;


        rb.MovePosition(transform.position + movimiento);
    }

    public void MoverseAbajo()
    {
        Vector3 movimiento = transform.up * Time.deltaTime * velocidad;


        rb.MovePosition(transform.position - movimiento);
    }


    public void movDerIzq()
    {
        if (transform.position.x < posObjetivoIzquierda.x)
        {
            moverseDerecha = true;
        }
        else if (transform.position.x > posObjetivoDerecha.x)
        {
            moverseDerecha = false;
        }



        if (moverseDerecha)
        {
            MoverseDerecha();
        }

        else
        {
            MoverseIzquierda();
        }

    }

    public void movArribaAbajo()
    {
        if (transform.position.y > posObjetivoArriba.y)
        {
            moverseArriba = false;
        }
        else if (transform.position.y < posObjetivoAbajo.y)
        {
            moverseArriba = true;
        }



        if (moverseArriba)
        {
            MoverseArriba();
        }

        else
        {
            MoverseAbajo();
        }
    }

}
