using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Target1 : MonoBehaviour
{
    private GameObject zonaBombas;
    private GameObject zonaBombasHielo;
    public GameObject bomba;
    public GameObject bombaHielo;
    private bool seInstancioBomba;
    public float timer;
    public float timer2;
    public bool bombasDeHielo = false;

    public float cdBombas = 1;
    void Start()
    {
        //transform.Find busca el objeto hijo del transform de este objeto

     
        Transform zonaBombasHieloTransform = transform.Find("zonaBombasHielo");
        if (zonaBombasHieloTransform != null)
        {
            zonaBombasHielo = zonaBombasHieloTransform.gameObject;
        }
        else
        {
            zonaBombasHielo = null;
        }

        Transform zonaBombasTransform = transform.Find("zonaBombas");
        if (zonaBombasTransform != null)
        {
            zonaBombas = zonaBombasTransform.gameObject;
        }
        else
        {
            zonaBombas = null;
        }


        seInstancioBomba = false;
        timer = 0;
        timer2 = 0;

        // Imprimir el valor de zonaBombas en la consola
        Debug.Log("El valor de zonaBombas es: " + zonaBombas);
    }

    // Update is called once per frame
    void Update()
    {
        if (zonaBombas != null)
        {
            if ( zonaBombas.GetComponent<Controller_zonaBombas>().playerEnZona == true)
            {
                timer += Time.deltaTime;
            }
        }
        if(zonaBombasHielo != null)
            {
            if (zonaBombasHielo.GetComponent<Controller_zonaBombas>().playerEnZona == true)
            {
                timer2 += Time.deltaTime;
            }
        }
        if ( timer>=1 && seInstancioBomba == false)
        {
            if (!bombasDeHielo)
            {
                hacerBombas();
                seInstancioBomba = true;
                Invoke("resetBombas", cdBombas);
            }
       
            timer = 0;
          
            
        }
           
        
        if (timer2 >= 1 && seInstancioBomba == false)
        {
            if (bombasDeHielo)
         
            {
            
                hacerBombasHielo();
                seInstancioBomba = true;
                Invoke("resetBombas", cdBombas);
            }
            timer2 = 0;
        }
    }

    private void hacerBombas()
    {
         GameObject bombaInstancia = Instantiate(bomba, transform.position+new Vector3(0,1,0), Quaternion.identity);

        if (zonaBombas.GetComponent<Controller_zonaBombas>().playerDerecha)
        {
            bombaInstancia.GetComponent<Rigidbody>().velocity += new Vector3(5, 10, 0);
        }
        else
        {
            bombaInstancia.GetComponent<Rigidbody>().velocity += new Vector3(-5, 10, 0);
        }

        Destroy(bombaInstancia, 1.0f);

    }

    private void hacerBombasHielo()
    {
        GameObject bombaHieloInstancia = Instantiate(bombaHielo, transform.position + new Vector3(0, -1, 0), Quaternion.identity);


        if (zonaBombasHielo.GetComponent<Controller_zonaBombas>().playerDerecha)
        {
            bombaHieloInstancia.GetComponent<Rigidbody>().velocity += new Vector3(1, -10, 0);
        }
        else
        {
            bombaHieloInstancia.GetComponent<Rigidbody>().velocity += new Vector3(-1, -10, 0);
        }
        Destroy(bombaHieloInstancia, 2f);

    }
    
    private void resetBombas()
    {
        seInstancioBomba = false;
    }
}
