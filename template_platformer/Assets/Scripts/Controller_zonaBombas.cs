using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_zonaBombas : MonoBehaviour
{
   public bool playerEnZona;
    private GameObject player;
    public bool playerDerecha;
    private void Start()
    {
        playerEnZona = false;
    }

    private void Update()
    {
        if (player != null)
        {
            if (player.transform.position.x > transform.position.x)
            {
                playerDerecha = true;
            }
            else
            {
                playerDerecha = false;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Controller_Player>())
        {
            playerEnZona = true;


            player = other.gameObject;
           
            

        }
       
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<Controller_Player>())
        {
          
            playerEnZona = false;
        }
    }
}
